import 'package:dartz/dartz.dart';

import '../../../../core/error/2failrules.dart';
import '../../../../core/usecase/6usecases.dart';
import '../enteties/1number_trivia.dart';
import '../repositories/3number_trivia_erpo.dart';

class GetConcreteNumber implements UseCase<NumberTrivia, Params> {
  final NumberTriviaRepository repository;

  GetConcreteNumber(this.repository);

  @override
  Future<Either<Failure, NumberTrivia>> call(
    Params params,
  ) async {
    return await repository.getConcreteNumber(params.number);
  }
}
