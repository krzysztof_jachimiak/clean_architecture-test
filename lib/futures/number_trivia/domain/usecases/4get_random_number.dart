import 'package:clear_project/core/error/2failrules.dart';
import 'package:clear_project/core/usecase/6usecases.dart';
import 'package:clear_project/futures/number_trivia/domain/enteties/1number_trivia.dart';
import 'package:clear_project/futures/number_trivia/domain/repositories/3number_trivia_erpo.dart';
import 'package:dartz/dartz.dart';

class GetRandomNumber implements UseCase<NumberTrivia, NoParams> {
  final NumberTriviaRepository repository;

  GetRandomNumber(this.repository);

  @override
  Future<Either<Failure, NumberTrivia>> call(NoParams params) async {
    // throw UnimplementedError();
    return await repository.getRandomNumber();
  }
}
