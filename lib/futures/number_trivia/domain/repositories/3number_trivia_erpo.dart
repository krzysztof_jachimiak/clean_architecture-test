import 'package:clear_project/core/error/2failrules.dart';
import 'package:clear_project/futures/number_trivia/domain/enteties/1number_trivia.dart';
import 'package:dartz/dartz.dart';

abstract class NumberTriviaRepository {
  Future<Either<Failure, NumberTrivia>> getConcreteNumber(int number);
  Future<Either<Failure, NumberTrivia>> getRandomNumber();
}
// Future <Either<Failrues,NumberTrivia>> pierwsza pozycja po Either  (zarówno)
//to że zawiera niepowodzenie a druga że zawiera wynik tak jest dla konkretnego
//nr jak również dla numenru wylkosowanego :)
