import 'package:clear_project/futures/number_trivia/data/models/7number_model.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:meta/meta.dart';

abstract class NumberTriviaLocalDataSource {
  Future<NumberModel> getLastNumber();

  Future<void> cacheNumberTrivia(NumberModel triviaToCasche);
}

class NumberTriviaLocalDataSourceImpl implements NumberTriviaLocalDataSource {
  final SharedPreferences sharedPreferences;

  NumberTriviaLocalDataSourceImpl({@required this.sharedPreferences});
  @override
  Future<void> cacheNumberTrivia(NumberModel triviaToCasche) {
    // TODO: implement cacheNumberTrivia
    throw UnimplementedError();
  }

  @override
  Future<NumberModel> getLastNumber() {
    // TODO: implement getLastNumber
    throw UnimplementedError();
  }
}
