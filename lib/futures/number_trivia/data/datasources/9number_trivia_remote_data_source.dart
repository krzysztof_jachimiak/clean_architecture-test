import 'package:clear_project/futures/number_trivia/data/models/7number_model.dart';

abstract class NumberTriviaRemoteDataSource {
  Future<NumberModel> getConcreteNumber(int number);
  Future<NumberModel> getRandomNumber();
}
