import 'package:clear_project/core/error/15exceptions.dart';
import 'package:clear_project/core/platform/11network_info.dart';
import 'package:clear_project/futures/number_trivia/data/datasources/9number_trivia_remote_data_source.dart';
import 'package:clear_project/futures/number_trivia/data/datasources/10number_trivia_local_data_source.dart';
import 'package:clear_project/futures/number_trivia/domain/enteties/1number_trivia.dart';
import 'package:clear_project/core/error/2failrules.dart';
import 'package:clear_project/futures/number_trivia/domain/repositories/3number_trivia_erpo.dart';

import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';

typedef Future<NumberTrivia> _Wybierz();

class NumberTriviaRepositoryImpl implements NumberTriviaRepository {
  final NumberTriviaRemoteDataSource remoteDataSource;
  final NumberTriviaLocalDataSource localDataSource;
  final NetworkInfo networkInfo;

  NumberTriviaRepositoryImpl({
    @required this.remoteDataSource,
    @required this.localDataSource,
    @required this.networkInfo,
  });

  @override
  Future<Either<Failure, NumberTrivia>> getConcreteNumber(
    int number,
  ) async {
    return await _getTrivia(() => remoteDataSource.getConcreteNumber(number));
  }

  @override
  Future<Either<Failure, NumberTrivia>> getRandomNumber() async {
    return await _getTrivia(() => remoteDataSource.getRandomNumber());
  }

  Future<Either<Failure, NumberTrivia>> _getTrivia(
      _Wybierz concreteOrRandom) async {
    if (await networkInfo.isConnected) {
      try {
        final remonteTrivia = await concreteOrRandom();
        localDataSource.cacheNumberTrivia(remonteTrivia);
        return Right(remonteTrivia);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      try {
        final localTrivia = await localDataSource.getLastNumber();
        return Right(localTrivia);
      } on CacheExcepions {
        return left(CacheFailure());
      }
    }
  }
}
