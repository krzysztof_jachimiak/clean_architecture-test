import 'package:clear_project/futures/number_trivia/domain/enteties/1number_trivia.dart';
import 'package:meta/meta.dart';

class NumberModel extends NumberTrivia {
  NumberModel({
    @required int number,
    @required String text,
  }) : super(number: number, text: text);

  factory NumberModel.fromJson(Map<String, dynamic> json) {
    return NumberModel(
      text: json['text'],
      number: (json['number'] as num).toInt(),
    );
  }
  Map<String, dynamic> toJson() {
    return {
      'text': text,
      'number': number,
    };
  }
}
