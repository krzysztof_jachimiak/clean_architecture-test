import 'package:equatable/equatable.dart';

abstract class Failure extends Equatable {
  Failure([List wlasnosci = const <dynamic>[]]) : super(wlasnosci);
}
//16 general failures

class ServerFailure extends Failure {}

class CacheFailure extends Failure {}
