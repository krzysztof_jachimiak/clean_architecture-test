import 'package:clear_project/core/platform/11network_info.dart';
import 'package:mockito/mockito.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:data_connection_checker/data_connection_checker.dart';

class MockdataConectionChecker extends Mock implements DataConnectionChecker {}

void main() {
  NetworkInfoImpl networkInfoImpl;
  MockdataConectionChecker mockdataConectionChecker;

  setUp(() {
    mockdataConectionChecker = MockdataConectionChecker();
    networkInfoImpl = NetworkInfoImpl(mockdataConectionChecker);
  });
  group('jest połączony', () {
    test(
        'powinien przekierować połączenie do modułu DataConectionChecker.haseConection',
        () async {
      final tHaseConectionFuture = Future.value(true);
      //arrage
      when(mockdataConectionChecker.hasConnection)
          .thenAnswer((_) => tHaseConectionFuture);
      //act
      final result = networkInfoImpl.isConnected;
      //assert
      verify(mockdataConectionChecker.hasConnection);
      expect(result, tHaseConectionFuture);
    });
  });
}
