import 'package:clear_project/core/usecase/6usecases.dart';
import 'package:clear_project/futures/number_trivia/data/models/7number_model.dart';
import 'package:clear_project/futures/number_trivia/domain/usecases/5get_concrete_number.dart';
import 'package:dartz/dartz.dart';
import 'package:clear_project/futures/number_trivia/domain/enteties/1number_trivia.dart';
import 'package:clear_project/futures/number_trivia/domain/repositories/3number_trivia_erpo.dart';
import 'package:mockito/mockito.dart';
import 'package:flutter_test/flutter_test.dart';
import 'dart:convert';
import '../../../../../fixtures/fixture_reader.dart';

class MocNumberTriviaRepository extends Mock implements NumberTriviaRepository {
}

void main() {
  GetConcreteNumber usecase;
  MocNumberTriviaRepository mocNumberTriviaRepository;
  setUp(() {
    mocNumberTriviaRepository = MocNumberTriviaRepository();
    usecase = GetConcreteNumber(mocNumberTriviaRepository);
  });
  final tNumber = 1;
  final tNumberTrivia = NumberTrivia(text: 'test', number: 1);

  test('powinien wziąć ciekawostki z repozytorium', () async {
    //arrage
    when(mocNumberTriviaRepository.getConcreteNumber(any))
        .thenAnswer((_) async => Right(tNumberTrivia));
    //act
    final rezult = await usecase(Params(number: tNumber));
    //assert
    expect(rezult, Right(tNumberTrivia));
    verify(mocNumberTriviaRepository.getConcreteNumber(tNumber));
    verifyNoMoreInteractions(mocNumberTriviaRepository);
  });

  final tNumberModel = NumberModel(number: 1, text: 'test text');

  test(
    'powinien być subklasą numbertrivia zawierającą entity',
    () async {
      expect(tNumberModel, isA<NumberTrivia>());
    },
  );
  group('fromJson', () {
    test('powinien zwrócić valid model gdy Json numer jest intem ', () async {
      final Map<String, dynamic> jsonMap = json.decode(fixture('trivia.json'));

      final result = NumberModel.fromJson(jsonMap);

      expect(result, tNumberModel);
    });
    test(
      'powinien zwrócić valid gdy  double ',
      () async {
        final Map<String, dynamic> jsonMap =
            json.decode(fixture('trivia_double.json'));

        final result = NumberModel.fromJson(jsonMap);

        expect(result, tNumberModel);
      },
    );
  });
  group('toJson', () {
    test('powinien zwrucić JsonMap rozszeżającą dane  ', () async {
      //act
      final result = tNumberModel.toJson();
      //assert
      final expectMap = {
        "text": "test text",
        "number": 1,
      };
      expect(result, expectMap);
    });
  });
}
