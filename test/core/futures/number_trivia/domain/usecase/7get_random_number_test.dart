import 'package:clear_project/futures/number_trivia/domain/usecases/4get_random_number.dart';
import 'package:dartz/dartz.dart';
import 'package:clear_project/futures/number_trivia/domain/enteties/1number_trivia.dart';
import 'package:clear_project/futures/number_trivia/domain/repositories/3number_trivia_erpo.dart';
import 'package:mockito/mockito.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:clear_project/core/usecase/6usecases.dart';

class MocNumberTriviaRepository extends Mock implements NumberTriviaRepository {
}

void main() {
  GetRandomNumber usecase;
  MocNumberTriviaRepository mocNumberTriviaRepository;
  setUp(() {
    mocNumberTriviaRepository = MocNumberTriviaRepository();
    usecase = GetRandomNumber(mocNumberTriviaRepository);
  });

  final tNumberTrivia = NumberTrivia(text: 'test', number: 1);

  test('should get trivia from the repository', () async {
    //arrage
    when(mocNumberTriviaRepository.getRandomNumber())
        .thenAnswer((_) async => Right(tNumberTrivia));
    //act
    final rezult = await usecase(NoParams());
    //assert
    expect(rezult, Right(tNumberTrivia));
    verify(mocNumberTriviaRepository.getRandomNumber());
    verifyNoMoreInteractions(mocNumberTriviaRepository);
  });
}
