import 'package:clear_project/futures/number_trivia/data/datasources/10number_trivia_local_data_source.dart';
import 'package:mockito/mockito.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MocSharedPreferences extends Mock implements SharedPreferences {
  void main() {
    NumberTriviaLocalDataSourceImpl dataSource;
    MocSharedPreferences mocSharedPreferences;
    setUpAll(() {
      mocSharedPreferences = MocSharedPreferences();
      dataSource = NumberTriviaLocalDataSourceImpl(
          sharedPreferences: mocSharedPreferences);
    });
  }
}
