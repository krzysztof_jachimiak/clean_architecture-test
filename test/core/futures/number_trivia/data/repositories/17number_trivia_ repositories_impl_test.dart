import 'package:clear_project/core/error/15exceptions.dart';
import 'package:clear_project/core/error/2failrules.dart';
import 'package:clear_project/core/platform/11network_info.dart';
import 'package:clear_project/futures/number_trivia/data/datasources/9number_trivia_remote_data_source.dart';
import 'package:clear_project/futures/number_trivia/data/datasources/10number_trivia_local_data_source.dart';
import 'package:clear_project/futures/number_trivia/data/models/7number_model.dart';
import 'package:clear_project/futures/number_trivia/data/repositories/8number_trivia_repository_impl.dart';
import 'package:clear_project/futures/number_trivia/domain/enteties/1number_trivia.dart';
import 'package:dartz/dartz.dart';
import 'package:mockito/mockito.dart';
import 'package:flutter_test/flutter_test.dart';

//MockRemonteDataSources - zaznaczone zdalne źródło danych
class MockRemonteDataSources extends Mock
    implements NumberTriviaRemoteDataSource {}

//zaznaczone lokalne źrudło danych
class MocklocalDataSources extends Mock implements NumberTriviaLocalDataSource {
}

//info o połączeniu sieciowym
class MockNetworkInfo extends Mock implements NetworkInfo {}

//ustawienie głównej metody tego testu
void main() {
  //co chcemy włożyć do testu
  NumberTriviaRepositoryImpl repositoryImpl;
  MockRemonteDataSources mockRemoteDataSources;
  MocklocalDataSources mockLocalDataSources;
  MockNetworkInfo mockNetworkInfo;
  // metoda konfiguracji
  setUp(() {
    mockRemoteDataSources = MockRemonteDataSources();
    mockLocalDataSources = MocklocalDataSources();
    mockNetworkInfo = MockNetworkInfo();
    repositoryImpl = NumberTriviaRepositoryImpl(
      remoteDataSource: mockRemoteDataSources,
      localDataSource: mockLocalDataSources,
      networkInfo: mockNetworkInfo,
    );
  });

  void runTestOnline(Function body) {
    group('urządzenie jest online', () {
      setUp(() {
        when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
      });
      body();
    });
  }

  void runTestOffline(Function body) {
    group('urządzenie jest offline', () {
      setUp(() {
        when(mockNetworkInfo.isConnected).thenAnswer((_) async => false);
      });
      body();
    });
  }

  group('getConcreteNumberTrivia', () {
    final tNumber = 1;
    final tNumberModel = NumberModel(number: tNumber, text: 'test trivia');
    final NumberTrivia tNumberTrivia = tNumberModel;
    test('powinien sprawdzić czy urządzenie jest online ', () async {
      //arrage
      when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
      //act
      repositoryImpl.getConcreteNumber(tNumber);
      //assert
      verify(mockNetworkInfo.isConnected);
    });

    runTestOnline(() {
      test(
          'powinien zwrócić dane zdalne, gdy wywołanie zdalnej usługi danych zakończy się powodzeniem',
          () async {
        //arrage
        when(mockRemoteDataSources.getConcreteNumber(any))
            .thenAnswer((_) async => tNumberModel);
        //act
        final result = await repositoryImpl.getConcreteNumber(tNumber);
        //assert
        verify(mockRemoteDataSources.getConcreteNumber(tNumber));
        expect(result, equals(Right(tNumberTrivia)));
      });
      test(
          'powinien zwrócić błąd serwera, gdy wywołanie zdalnej usługi danych zakończy się porażką',
          () async {
        //arrage
        when(mockRemoteDataSources.getConcreteNumber(any))
            .thenThrow(ServerException());
        //act
        final result = await repositoryImpl.getConcreteNumber(tNumber);
        //assert
        verify(mockRemoteDataSources.getConcreteNumber(tNumber));
        verifyZeroInteractions(mockLocalDataSources);
        expect(result, equals(Left(ServerFailure())));
      });

      test(
          'powinien buforować dane lokalnie gdy wywołanie zdalnej usługi danych zakończy się powodzeniem',
          () async {
        //arrage
        when(mockRemoteDataSources.getConcreteNumber(any))
            .thenAnswer((_) async => tNumberModel);
        //act
        await repositoryImpl.getConcreteNumber(tNumber);
        //assert
        verify(mockRemoteDataSources.getConcreteNumber(tNumber));
        verify(mockLocalDataSources.cacheNumberTrivia(tNumberModel));
      });
    });
    runTestOffline(() {
      setUp(() {
        when(mockNetworkInfo.isConnected).thenAnswer((_) async => false);
      });
      test(
          'powinien zwrócić ostatnie dane z pamięci podręcznej, gdy dane są obecne w pamięci podręcznej',
          () async {
        //arrage
        when(mockLocalDataSources.getLastNumber())
            .thenAnswer((_) async => tNumberModel);
        //act
        final result = await repositoryImpl.getConcreteNumber(tNumber);
        //assert
        verifyZeroInteractions(mockRemoteDataSources);
        verify(mockLocalDataSources.getLastNumber());
        expect(result, equals(Right(tNumberTrivia)));
      });
      test(
        'Powinien zwrucić błąd pamięci Podręcznej gdy w pamięci nie ma danych ',
        () async {
          // arrange
          when(mockLocalDataSources.getLastNumber())
              .thenThrow(CacheExcepions());
          // act
          final result = await repositoryImpl.getConcreteNumber(tNumber);
          // assert
          verifyZeroInteractions(mockRemoteDataSources);
          verify(mockLocalDataSources.getLastNumber());
          expect(result, equals(Left(CacheFailure())));
        },
      );
    });
  });
  group('getRandomNumberTrivia', () {
    final tNumberModel = NumberModel(number: 123, text: 'test trivia');
    final NumberTrivia tNumberTrivia = tNumberModel;
    test('powinien sprawdzić czy urządzenie jest online ', () async {
      //arrage
      when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
      //act
      repositoryImpl.getRandomNumber();
      //assert
      verify(mockNetworkInfo.isConnected);
    });

    runTestOnline(() {
      test(
          'powinien zwrócić dane zdalne, gdy wywołanie zdalnej usługi danych zakończy się powodzeniem',
          () async {
        //arrage
        when(mockRemoteDataSources.getRandomNumber())
            .thenAnswer((_) async => tNumberModel);
        //act
        final result = await repositoryImpl.getRandomNumber();
        //assert
        verify(mockRemoteDataSources.getRandomNumber());
        expect(result, equals(Right(tNumberTrivia)));
      });
      test(
          'powinien zwrócić błąd serwera, gdy wywołanie zdalnej usługi danych zakończy się porażką',
          () async {
        //arrage
        when(mockRemoteDataSources.getRandomNumber())
            .thenThrow(ServerException());
        //act
        final result = await repositoryImpl.getRandomNumber();
        //assert
        verify(mockRemoteDataSources.getRandomNumber());
        verifyZeroInteractions(mockLocalDataSources);
        expect(result, equals(Left(ServerFailure())));
      });

      test(
          'powinien buforować dane lokalnie gdy wywołanie zdalnej usługi danych zakończy się powodzeniem',
          () async {
        //arrage
        when(mockRemoteDataSources.getRandomNumber())
            .thenAnswer((_) async => tNumberModel);
        //act
        await repositoryImpl.getRandomNumber();
        //assert
        verify(mockRemoteDataSources.getRandomNumber());
        verify(mockLocalDataSources.cacheNumberTrivia(tNumberModel));
      });
    });
    runTestOffline(() {
      setUp(() {
        when(mockNetworkInfo.isConnected).thenAnswer((_) async => false);
      });
      test(
          'powinien zwrócić ostatnie dane z pamięci podręcznej, gdy dane są obecne w pamięci podręcznej',
          () async {
        //arrage
        when(mockLocalDataSources.getLastNumber())
            .thenAnswer((_) async => tNumberModel);
        //act
        final result = await repositoryImpl.getRandomNumber();
        //assert
        verifyZeroInteractions(mockRemoteDataSources);
        verify(mockLocalDataSources.getLastNumber());
        expect(result, equals(Right(tNumberTrivia)));
      });
      test(
        'Powinien zwrucić błąd pamięci Podręcznej gdy w pamięci nie ma danych ',
        () async {
          // arrange
          when(mockLocalDataSources.getLastNumber())
              .thenThrow(CacheExcepions());
          // act
          final result = await repositoryImpl.getRandomNumber();
          // assert
          verifyZeroInteractions(mockRemoteDataSources);
          verify(mockLocalDataSources.getLastNumber());
          expect(result, equals(Left(CacheFailure())));
        },
      );
    });
  });
}
